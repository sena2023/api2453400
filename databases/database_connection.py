"""Clase para conexion mysql"""
import os
from dotenv import load_dotenv
import pymysql

load_dotenv()

class database_connection:
    def __init__(self):
        self.host = os.environ.get('DB_HOST') or '127.0.0.1'
        self.port = os.environ.get('DB_PORT') or 3306
        self.user = os.environ.get('DB_USER') or 'root'
        self.password = os.environ.get('DB_PASSWORD') or 'password'
        self.database = os.environ.get('DB_DATABASE') or 'database'
        self.connection = self.connect()
        
    def connect(self):
        return pymysql.connect(host=self.host, port = int(self.port), user=self.user, password = self.password, database= self.database)
    
    def insert(self, table, data):
        try:
            cursor = self.connection.cursor()
            fields = ', '.join(data.keys())
            values = tuple(data.values())
            placeholders = ', '.join(['%s'] * len(values))
            query = f"INSERT INTO {table} ({fields}) VALUES ({placeholders})"
            cursor.execute(query, values)
            return self
        except Exception as e:
            print("Eror al insertar los datos:", str(e))
            return False
        
    def save(self):
        try:
            self.connection.commit()
            self.connection.close()
            return self
        except Exception as e:
            print("Eror al commit de los datos:", str(e))
            return False
        
            
        