from databases.database_connection import database_connection
import bcrypt

def create(request):
    if request.method == 'GET':
        return "nada"
    if request.method == 'POST':
        request_data = request.get_json()
        #isset request parameters
        if 'fullname' not in request_data:
            return 'Campo Nombre Completo es Obligatorio'
        if 'email' not in request_data:
            return 'Campo de Email es Obligatorio'
        if 'password' not in request_data:
            return 'Campo de clave es Obligatorio'
        
        #len
        if len(request_data['fullname']) == 0 or len(request_data['email']) == 0 or len(request_data['password']) == 0:
            return "Todos los campos deben tener contenido"
        sal = bcrypt.gensalt()
        data = {
            'fullname' : request_data['fullname'],
            'email' : request_data['email'],
            'password' : str(bcrypt.hashpw(request_data['password'].encode('utf-8'), sal))
        }
        conn = database_connection()
        insert = conn.insert('users', data).save()
        if insert:
            return "Usuario Creado Exitosamente"
        else:
            return "Error al Crear el Usuario"
        
        
        
        #Persistencia de datos () patron de diseño builder
    
    
# fullname string(218)
# email string(120), => nickname + @ + dominio + tld (.com, net , ....)
# password string(191) 