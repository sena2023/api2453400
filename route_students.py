from flask import Blueprint, request
from controllers.users import create_users

route_students = Blueprint('route_students', __name__)

@route_students.route('/api/v1/users', methods=['GET','POST'])
def users():
    return create_users.create(request)
    # if request.method == 'GET':
    #     return 'Esta ruta no permite el metodo get'
    # if request.method == 'POST':
    #     if request.json["fullname"]:
    #         return "xxxxx"
    #     else:
    #         return "El campo de Nombre Completo es Obligatorio"
        
