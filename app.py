"""Import the framework flask and other components"""
import os
from flask import Flask, request
from dotenv import load_dotenv
from route_students import route_students

load_dotenv()

port = os.getenv('APP_PORT')

app = Flask(__name__)

#Route students
app.register_blueprint(route_students)

@app.route('/')
def index():
    return "Hola SENA"


if __name__ == '__main__':
    app.run(port=port, debug=True)
