API - Estudiantes SENA
===
Abstract:xxx
## Papar Information
- Title:  `paper name`
- Authors:  `A`,`B`,`C`
- Preprint: [https://arxiv.org/abs/xx]()
- Full-preprint: [paper position]()
- Video: [video position]()

## Install & Dependence
- python3


## Directory Hierarchy
```
|
|—— .env
|—— .env.example
|—— .gitignore
|—— app.py
|—— controllers
|    |—— users
|        |—— __init__.py
|        |—— create_users.py
|—— databases
|    |—— __init__.py
|    |—— database_connection.py
|—— requirements.txt
|—— route_students.py
|—— test_route
|    |—— users.http
```
## Code Details
### Tested Platform
- software
  ```
  OS: Debian unstable (May 2021), Ubuntu LTS
  Python: 3.8.5 (anaconda)
  PyTorch: 1.7.1, 1.8.1
  ```
- hardware
  ```
  CPU: Intel Xeon 6226R
  GPU: Nvidia RTX3090 (24GB)
  ```
### Hyper parameters
```
```
## References

  
## License

## Citing
If you use xxx,please use the following BibTeX entry.
```
```
